Test 1: Observe accuracy, recall and f1-score for 100 epoch

Goal: observe the sufficient number of epochs + observe the impact of the number of labels on the scores

So we will train a model with all the QAs and compare the results for a model trained on one QA
The model with all QAs is better than the one trained with a QA.

Batch_size = 64
"size_data_with_no_label": 0.3
"test_size": 0.33

nb_epoch = 100 

Best score f1_score : 
All QA model: performance, usability, security
Model with 1 best QA: compatibility,reliability,maintainability,portability

Be careful after another train of the global model 
Better overall model: compatibility, usability, security, security, maintainability
Model with 1 best QA: performance, reliability, portability


usability remains very poor, exploring data can be too vague


Result: little use to split the model into several models because there is no real gain.

------------> QUESTIONS HERE (please confirm where there are question marks, and correct where necessary)
## Graphs:
First set (11:) are: general-model trained on all QA
Second set (8:): for each, first one is general-model and second one is specialised model per QA
Subsequent ones (93: ->): are various combinations
Set starting at (145:) are general-model (batch64) compared to batch 32 (general-model) 
From (29:) Ignore

_____________________________________________________

Test 2 : Eval with K_fold approach
K-Folds cross-validator
Provides train/test indices to split data in train/test sets. Split dataset into k consecutive folds (without shuffling by default).
Each fold is then used once as a validation while the k - 1 remaining folds form the training set.
We compare result from first test on batch 32 to k_fold 10 score.

With this validation tool we can have the real eval score. 

## Graphs:
Second set (27:) first graph are  32, second are k-fold 10

__________________________________________________________


Test 3 : Check Most Common word 
We used the observations of the batch_32 model.
Objective: to better understand them and why the model is less good for certain QAs.
Obs:
We first compare the train against all the data to see if the most used words are the same.
This allows us to ensure that the train looks like the overall data.

We compare the TPs with the train and we can see that it is the same words that are most relevant for the QAs best categorized by the model.
We can see that the NF do not have the same most used words.
The same observation applies to FP.

We can see that for usability, there are no words that stand out. This means that the QA is too vague.

__________________________________________________________


Test 4: amount of data
We take a different train dataset from the test.
We shuffle the train.
We split it into train subassemblies.
We train the model with a dataset of different size (100,200,300,300,400,500)
We test on the same test dataset (Different from the train)
Goal: to show the importance of collecting more data to improve the model

Obs:
After observation, our initial hypothesis is confirmed. We can see a correlation between the size of the train data set and the performance of the model. 
The more data we have, the more efficient it is. 
We have not yet reached its maximum learning threshold.


__________________________________________________________

Test 5: Modify observed with a higher or lower decision cap (0.4 or 0.6)
Heading 0.6: on the global evaluation we gain accuracy in general.

Best F1-score with 0,6 : Compatibility(low),Security(high),portability(low)

We see that a single decision course for all AQs may not be a good thing. We should decide a different decision course by QA. It will be a meta-parameter to be optimized.

__________________________________________________________

Test 6: identify most common words in stories with or without QA

Generate graphs of word occurrences for 10, 15 and 20 most common words.

__________________________________________________________

Test 7: Identify user contains a QA or not

Goal: check to what extend the model is able to label stories that contains a QA or not (regardless of the QA), i.e, the yes/no column for "contains QA" from labelled dataset. 

Generate graph of k-fold 10


