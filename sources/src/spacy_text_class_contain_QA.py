
"""
Spacy model used for QA detection in a user story.
"""

from __future__ import unicode_literals, print_function
import plac
import random
from pathlib import Path

import spacy
from spacy.util import minibatch, compounding

import pandas as pd
import ast

from helpers.args import get_args
from helpers.utils import dump_json, load_json, create_dir



def main(config):

    n_iter = config["model_train"]["n_epoch"]
    n_texts = config["model_train"]["n_texts"]
    name_model = config["model_train"]["name"]
    
    batch_size = config["model_train"]["batch_size"]
    
    try:
        list_QA =  config["model_train"]["list_label"]
    except KeyError:
        list_QA =  config["data"]["list_QA"]

    try:
        path_save_obs = config["model_train"]["path_save_obs"]

        if(config["model_train"]["k_fold"] > 0):
            path_save_obs_ = Path(path_save_obs+"/"+name_model)
            if not path_save_obs_.exists():
                path_save_obs_.mkdir()
            path_save_obs=path_save_obs+name_model
            name_model = name_model+"_fold_"+str(config["model_train"]["id_test_k_fold"])

    except KeyError:
        path_save_obs = None
    
    try:
        model = config["model_train"]["load_model"]
    except KeyError:
        model = None
    
    try:
        output_dir = config["model_train"]["save_model_path"]+config["model_train"]["name"]
        
    except KeyError:
        output_dir = None
        
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.load('en_core_web_lg')  # create blank Language class
        print("Created blank 'en' model")

    # add the text classifier to the pipeline if it doesn't exist
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if 'textcat' not in nlp.pipe_names:
        textcat = nlp.create_pipe('textcat')
        nlp.add_pipe(textcat, last=True)
    # otherwise, get it, so we can add labels to it
    else:
        textcat = nlp.get_pipe('textcat')

    # add label to text classifier
    for qa in list_QA:
        textcat.add_label(qa)

    # load the IMDB dataset
    print("Loading US data with QA...")
    (train_texts, train_cats), (dev_texts, dev_cats) = load_data(config)
    print("Using {} examples ({} training, {} evaluation)"
          .format(len(train_texts)+ len(dev_texts), len(train_texts), len(dev_texts)))
    train_data = list(zip(train_texts,
                          [{'cats': cats} for cats in train_cats]))

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'textcat']
    list_eval = []
    list_epoch = []
    with nlp.disable_pipes(*other_pipes):  # only train textcat
        optimizer = nlp.begin_training()
        print("Training the model...")
        print('{:^5}\t{:^5}\t{:^5}\t{:^5}'.format('LOSS', 'P', 'R', 'F'))
        for i in range(n_iter):
            print("Epoch ",i)
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(train_data, size=compounding(4., batch_size, 1.1))

            for batch in batches:
                
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.2,
                           losses=losses)

            with textcat.model.use_params(optimizer.averages):
                # evaluate on the dev data split off in load_data()
                scores,_,lab_stat_pred = evaluate(list_QA,nlp.tokenizer, textcat, dev_texts, dev_cats,config["model_train"]["ratio"])
            lab_stat_pred["eval_global"] = scores
            list_eval.append(lab_stat_pred)
            list_epoch.append(i)
            print('{0:.3f}\t{1:.3f}\t{2:.3f}\t{3:.3f}'  # print a simple table
                  .format(losses['textcat'], scores['textcat_p'],
                          scores['textcat_r'], scores['textcat_f']))
    df_eval = pd.DataFrame()
    df_eval['eval'] = list_eval
    df_eval['epoch'] = list_epoch

    

    # test the trained model
    test_text = "As a Broker user, I want  to help create content mockups, so that I can submit my data efficiently."
    doc = nlp(test_text)
    print(test_text, doc.cats)

    with textcat.model.use_params(optimizer.averages):
        scores,dic_pred_sentence,_ = evaluate(list_QA,nlp.tokenizer, textcat, dev_texts, dev_cats,config["model_train"]["ratio"])
        scores,dic_pred_sentence_train,_ = evaluate(list_QA,nlp.tokenizer, textcat, train_texts, train_cats,config["model_train"]["ratio"])

    
    if path_save_obs is not None:
        path_save_obs = Path(path_save_obs+"/"+name_model)
        if not path_save_obs.exists():
            path_save_obs.mkdir()
        save_obs_sent(dic_pred_sentence,list_QA,path_save_obs,name_model,type_obs="test")
        save_obs_sent(dic_pred_sentence_train,list_QA,path_save_obs,name_model,type_obs="train")

        df_eval.to_csv(str(path_save_obs)+"/eval_"+str(name_model)+".csv")
        print("Saved observation devset", path_save_obs)


    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        with nlp.use_params(optimizer.averages):
            nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        doc2 = nlp2(test_text)
        print(test_text, doc2.cats)
   


def load_data(config):
    """
    Loading training and test data. 
    For each user story, we will indicate whether or not it contains an QA in y in the format {QA : True}
    """

    if (config["model_train"]["k_fold"]>0):
        # When we want to evaluate by cross validation
        all_data = pd.read_csv(config["data"]["all_data_path"])
        train_data,test_data= split_kfold(all_data,config["model_train"]["k_fold"],config["model_train"]["id_test_k_fold"])

    else:
        # Partition off part of the train data for evaluation
        train_data = pd.read_csv(config["data"]["data_train_path"])

        test_data = pd.read_csv(config["data"]["data_test_path"])
    X_train = list(train_data["User story"])
    y_train = [{'QA': bool(y)} for y in list(train_data["contains_QA_bool"])]

    X_test = list(test_data["User story"])
    y_test = [{'QA': bool(y)} for y in list(test_data["contains_QA_bool"])]  
    print("data loaded")
    return (X_train, y_train), (X_test, y_test)


def evaluate(list_QA,tokenizer, textcat, texts, cats,ratio):

    docs = (tokenizer(text) for text in texts)

    lab_stat_pred = {}
    for qa in list_QA:
        lab_stat_pred[qa] = {
            "tp" : 0.0 ,  # True positives
            "fp" : 1e-8 , # False positives
            "fn" : 1e-8,  # False negatives
            "tn" : 0.0 ,  # True negatives
        }

    list_sent_pred = {}
    for qa in list_QA:
        list_sent_pred[qa] = {
            "tp" : [] ,  # list True positives (sentences,prediction value)
            "fp" : [] , # list False positives (sentences,prediction value)
            "fn" : [],  # list False  (sentences,prediction value)
            "tn" : [] ,  # list True negatives (sentences,prediction value)
        }

    tp = 0.0   # True positives
    fp = 1e-8  # False positives
    fn = 1e-8  # False negatives
    tn = 0.0   # True negatives
    for i, doc in enumerate(textcat.pipe(docs)):
        gold = cats[i]
        for label, score in doc.cats.items():
            if label not in gold:
                continue
            if score >= ratio and gold[label] >= ratio:
                lab_stat_pred[label]["tp"] += 1.
                list_sent_pred[label]["tp"].append({"US":doc,"pred":score})
                tp += 1.
            elif score >= ratio and gold[label] < ratio:
                lab_stat_pred[label]["fp"] += 1.
                list_sent_pred[label]["fp"].append({"US":doc,"pred":score})
                fp += 1.
            elif score < ratio and gold[label] < ratio:
                lab_stat_pred[label]["tn"] += 1.
                list_sent_pred[label]["tn"].append({"US":doc,"pred":score})
                tn += 1
            elif score < ratio and gold[label]>= ratio:
                lab_stat_pred[label]["fn"] += 1.
                list_sent_pred[label]["fn"].append({"US":doc,"pred":score})
                fn += 1
            
    for qa in list_QA:
        lab_stat_pred[qa]["tot"] = lab_stat_pred[qa]["tp"] + lab_stat_pred[qa]["fn"]
        lab_stat_pred[qa]["eval"] = evaluate_one_lab(lab_stat_pred[qa]["tp"],lab_stat_pred[qa]["fp"], lab_stat_pred[qa]["tn"],lab_stat_pred[qa]["fn"])

        print(qa)
        print(lab_stat_pred[qa])
    
    return evaluate_one_lab(tp,fp,tn,fn) ,list_sent_pred, lab_stat_pred


def evaluate_one_lab(tp,fp,tn,fn):
    """
    Calculates precision, recall and F1_score.    
    """
    if((tp + fp) != 0):
        precision = tp / (tp + fp)
    else: precision = 0
    if((tp + fn) != 0):
        recall = tp / (tp + fn)
    else: recall = 0
    if((precision + recall) != 0):
        f_score = 2 * (precision * recall) / (precision + recall)
    else: f_score = 0
    return {'textcat_p': precision, 'textcat_r': recall, 'textcat_f': f_score}

def save_obs_sent(dic_QA_pred,list_QA,path,name_model,type_obs=""):
     """
    Backup in separate CSV files the US considered as TP, TN, FP and FN 
    """
    df_tp = pd.DataFrame()
    df_tn = pd.DataFrame()
    df_fp = pd.DataFrame()
    df_fn = pd.DataFrame()
    
    for qa in list_QA:
        df = pd.DataFrame(dic_QA_pred[qa]["tp"])
        if(len(df)>0):
            df["label"] = list( [qa for i in range(0,len(df))])
            df_tp = pd.concat([df_tp,df])

        df = pd.DataFrame(dic_QA_pred[qa]["tn"])
        if(len(df)>0):
            df["label"] = list([qa for i in range(0,len(df))])
            df_tn = pd.concat([df_tn,df])

        df = pd.DataFrame(dic_QA_pred[qa]["fp"])
        if(len(df)>0):
            df["label"] = list([qa for i in range(0,len(df))])
            df_fp = pd.concat([df_fp,df])

        df = pd.DataFrame(dic_QA_pred[qa]["fn"])
        if(len(df)>0):
            df["label"] = list([qa for i in range(0,len(df))])
            df_fn = pd.concat([df_fn,df])
    print("path")
    print(path)
    print(name_model)
    print(str(path)+"tp_"+str(name_model)+".csv")

    df_tp.to_csv(str(path)+"/"+type_obs+"_tp_"+str(name_model)+".csv")
    df_tn.to_csv(str(path)+"/"+type_obs+"_tn_"+str(name_model)+".csv")
    df_fp.to_csv(str(path)+"/"+type_obs+"_fp_"+str(name_model)+".csv")
    df_fn.to_csv(str(path)+"/"+type_obs+"_fn_"+str(name_model)+".csv")


def split_kfold(df,k_fold,id_test_fold):
    """
    Decomposes the dataset into the number of fold indicated in the config file.
    Returns a test dataset composed of the fold having the id_test and a train dataset composed of all other folds
    """
    size_fold = len(df)//k_fold
    print(size_fold)
    df_test = df[id_test_fold*size_fold:(id_test_fold+1)*size_fold]
    df_train = df[~df["User story"].isin(df_test["User story"]) ]
    print("all ",len(df))

    print("test ",len(df_test))
    print("train ",len(df_train))


    return df_train ,df_test






if __name__ == '__main__':
    args = get_args()
    config = load_json(args.config)
    if(config["model_train"]["k_fold"]>0):
        print("kfold")
        for i in range(0,config["model_train"]["k_fold"]):
            print("K_FOLD : ",i)
            config["model_train"]["id_test_k_fold"] = i
            main(config)
            print("-------------",i)


    else:
        main(config)