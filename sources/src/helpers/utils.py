import json
import os
from spacy.util import compounding, minibatch


def create_dir(dir):
    """
    dir - a directory to create if this directory is not found
    :param dir:
    :return exit_code: 0:success -1:failed
    """
    try:
        if not os.path.exists(dir):
            os.makedirs(dir)
        return 0
    except Exception as err:
        log.error("Creating directories error: {0}".format(err))
    exit(-1)


def load_json(path):
    with open(path) as json_file:
        o_file = json_file.read()
    return json.loads(o_file)


def dump_json(data, path, sort_keys=False, indent=None):
    with open(path, 'w') as json_file:
        json.dump(data, json_file, sort_keys=sort_keys, indent=indent)


def get_batches(train_data, max_batch_size=16):
    if len(train_data) < 1000:
        max_batch_size /= 2
    if len(train_data) < 500:
        max_batch_size /= 2
    batch_size = compounding(1, max_batch_size, 1.001)
    batches = minibatch(train_data, size=batch_size)
    return batches


