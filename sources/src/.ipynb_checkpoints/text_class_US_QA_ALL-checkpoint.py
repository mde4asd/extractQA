#!/usr/bin/env python
# coding: utf8
"""Train a convolutional neural network text classifier on the
IMDB dataset, using the TextCategorizer component. The dataset will be loaded
automatically via Thinc's built-in dataset loader. The model is added to
spacy.pipeline, and predictions are available via `doc.cats`. For more details,
see the documentation:
* Training: https://spacy.io/usage/training
Compatible with: spaCy v2.0.0+
"""
from __future__ import unicode_literals, print_function
import plac
import random
from pathlib import Path
import thinc.extra.datasets

import spacy
from spacy.util import minibatch, compounding

import pandas as pd
import ast

from helpers.args import get_args
from helpers.utils import dump_json, load_json, create_dir



def main(config,model=None, output_dir=None, n_iter=100, n_texts=2000):
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.load('en_core_web_lg')  # create blank Language class
        print("Created blank 'en' model")

    # add the text classifier to the pipeline if it doesn't exist
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if 'textcat' not in nlp.pipe_names:
        textcat = nlp.create_pipe('textcat')
        nlp.add_pipe(textcat, last=True)
    # otherwise, get it, so we can add labels to it
    else:
        textcat = nlp.get_pipe('textcat')

    # add label to text classifier
    for qa in config["data"]["list_QA"]:
        textcat.add_label(qa)

    # load the IMDB dataset
    print("Loading IMDB data...")
    (train_texts, train_cats), (dev_texts, dev_cats) = load_data(config)
    print("Using {} examples ({} training, {} evaluation)"
          .format(n_texts, len(train_texts), len(dev_texts)))
    train_data = list(zip(train_texts,
                          [{'cats': cats} for cats in train_cats]))

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'textcat']
    with nlp.disable_pipes(*other_pipes):  # only train textcat
        optimizer = nlp.begin_training()
        print("Training the model...")
        print('{:^5}\t{:^5}\t{:^5}\t{:^5}'.format('LOSS', 'P', 'R', 'F'))
        for i in range(n_iter):
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(train_data, size=compounding(4., 32., 1.1))

            for batch in batches:
                
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.2,
                           losses=losses)

            with textcat.model.use_params(optimizer.averages):
                # evaluate on the dev data split off in load_data()
                scores = evaluate(config,nlp.tokenizer, textcat, dev_texts, dev_cats)
            print('{0:.3f}\t{1:.3f}\t{2:.3f}\t{3:.3f}'  # print a simple table
                  .format(losses['textcat'], scores['textcat_p'],
                          scores['textcat_r'], scores['textcat_f']))

    # test the trained model
    test_text = "As a Broker user, I want  to help create content mockups, so that I can submit my data efficiently."
    doc = nlp(test_text)
    print(test_text, doc.cats)

    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        with nlp.use_params(optimizer.averages):
            nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        doc2 = nlp2(test_text)
        print(test_text, doc2.cats)


def load_data(config):
    # Partition off part of the train data for evaluation
    train_data = pd.read_csv(config["data"]["data_train_path"])

    X_train = list(train_data["User story"])
    y_train = [ast.literal_eval(lab) for lab in list(train_data["label_json"])]   

    test_data = pd.read_csv(config["data"]["data_test_path"])

    X_test = list(test_data["User story"])
    y_test = [ast.literal_eval(lab) for lab in list(test_data["label_json"])]   

    return (X_train, y_train), (X_test, y_test)


def evaluate(config,tokenizer, textcat, texts, cats):
    docs = (tokenizer(text) for text in texts)

    lab_stat_pred = {}
    for qa in config["data"]["list_QA"]:
        lab_stat_pred[qa] = {
            "tp" : 0.0 ,  # True positives
            "fp" : 1e-8 , # False positives
            "fn" : 1e-8,  # False negatives
            "tn" : 0.0 ,  # True negatives
        }

    tp = 0.0   # True positives
    fp = 1e-8  # False positives
    fn = 1e-8  # False negatives
    tn = 0.0   # True negatives
    for i, doc in enumerate(textcat.pipe(docs)):
        gold = cats[i]
        for label, score in doc.cats.items():
            if label not in gold:
                continue
            if score >= 0.5 and gold[label] >= 0.5:
                lab_stat_pred[label]["tp"] += 1.
                tp += 1.
            elif score >= 0.5 and gold[label] < 0.5:
                lab_stat_pred[label]["fp"] += 1.
                fp += 1.
                print("fp : ",label," ",doc)
            elif score < 0.5 and gold[label] < 0.5:
                lab_stat_pred[label]["tn"] += 1.
                tn += 1
            elif score < 0.5 and gold[label]>= 0.5:
                lab_stat_pred[label]["fn"] += 1.

                print("fn : ",label," ",doc)
                fn += 1
    print("fp",fp)
    print("tp",tp)
    print("fn",fn)
    print("tn",tn)
    for qa in config["data"]["list_QA"]:
        lab_stat_pred[qa]["tot"] = lab_stat_pred[qa]["tp"] + lab_stat_pred[qa]["fn"]
        lab_stat_pred[qa]["eval"] = evaluate_one_lab(lab_stat_pred[qa]["tp"],lab_stat_pred[qa]["fp"], lab_stat_pred[qa]["tn"],lab_stat_pred[qa]["fn"])

        print(qa)
        print(lab_stat_pred[qa])
    return evaluate_one_lab(tp,fp,tn,fn)


def evaluate_one_lab(tp,fp,tn,fn):
    if((tp + fp) != 0):
        precision = tp / (tp + fp)
    else: precision = 0
    if((tp + fn) != 0):
        recall = tp / (tp + fn)
    else: recall = 0
    if((precision + recall) != 0):
        f_score = 2 * (precision * recall) / (precision + recall)
    else: f_score = 0
    return {'textcat_p': precision, 'textcat_r': recall, 'textcat_f': f_score}


if __name__ == '__main__':
    args = get_args()
    config = load_json(args.config)
    main(config)
