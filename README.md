# extractQA - Extraction of Quality Attributes from User Stories

This repository contains the sources for the sub-project dealing with the extraction of Quality attributes from User Stories.

It is part of the umbrella project Machine Learning for Agile Software Development (ML4ASD)

## What's in there

`|_ data` annotated user stories (from [Dalpiaz](https://data.mendeley.com/datasets/7zbk8zsd8y/1))

`|_ sources` various scripts and notebooks to reproduce the outcome of the paper (includes its own `README` file)

## Published paper

This work has been published at [MARCH@ICSA 2019](https://doi.org/10.1109/ICSA-C.2019.00031) and [ECSA 2019](https://doi.org/10.1007/978-3-030-29983-5_6).
